<?php

/*
 * Use Taxonomy Job Sidebar
 *
**/ 
add_action( 'genesis_header','fuse_job_taxonomy_genesis_header' );
function fuse_job_taxonomy_genesis_header () {
	remove_action( 'genesis_sidebar', 'ss_do_sidebar' );
	remove_action( 'genesis_sidebar', 'genesis_do_sidebar' );
	add_action('genesis_sidebar','fuse_job_taxonomy_sidebar');
}

function fuse_job_taxonomy_sidebar(){
	dynamic_sidebar( 'sidebar-job-taxonomy' );
}




/*
 * Add job category list before loop, but after archive title
 *
**/

// add_action ('genesis_before_loop','fuse_jobs_category_list', 99);



/*
 * Customise the taxonomy title / description e.g. "Hotel and Catering jobs in Sussex"
 *
**/
remove_action( 'genesis_before_loop', 'genesis_do_taxonomy_title_description', 15 );
add_action( 'genesis_before_loop', 'fuse_jobs_do_taxonomy_title_description', 15 );
function fuse_jobs_do_taxonomy_title_description () {

	$headline = $intro_text = '';
	$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
	
	if ( $term->meta['headline'] ) {
		$headline = sprintf( '<h1 %s>%s</h1>', genesis_attr( 'archive-title' ), strip_tags( $term->meta['headline'] ) );
	} else {
		if ( genesis_a11y( 'headings' ) ) {
			$headline = sprintf( '<h1 %s>%s</h1>', genesis_attr( 'archive-title' ), strip_tags( $term->name ) );
		} else {
			$headline = sprintf( '<h1 %s>%s</h1>', genesis_attr( 'archive-title' ), strip_tags( $term->name ) . ' jobs in Sussex' );
		}
	}

	if ( $term->meta['intro_text'] )
		$intro_text = apply_filters( 'genesis_term_intro_text_output', $term->meta['intro_text'] );

	if ( $headline || $intro_text )
		printf( '<div %s>%s</div>', genesis_attr( 'taxonomy-archive-description' ), $headline. $intro_text );

}















/*





// Set Schema
// turn the schema type of each individual entry on the page into a job
add_filter( 'genesis_attr_entry', 'phut_schema_job', 20 );

// replace the normal “headline” itemprop for the entry title with “title”, as required by this specific schema
add_filter( 'genesis_attr_entry-title', 'phut_itemprop_title', 20 );

// change the itemprop of the entry content to “description”, instead of “text”, which is the default for a blog post;
add_filter( 'genesis_attr_entry-content', 'phut_itemprop_description', 20 );


// make sure the link in the headline has the itemprop=”url” needed; // archives only??
// add_filter( 'genesis_post_title_output', 'phut_title_link_schema', 20 );


// remove the overall schema.org type of the page as that would confuse the search engine.
add_filter( 'genesis_attr_content', 'phut_schema_empty', 20 );














//* Remove default loop
remove_action( 'genesis_loop', 'genesis_do_loop' );

add_action( 'genesis_loop', 'fuse_jobs_taxonomy_loop' );


function fuse_jobs_taxonomy_loop(){
	
}
*/



remove_action( 'genesis_loop_else', 'genesis_do_noposts' );
add_action('genesis_loop_else', 'fuse_jobs_do_noposts' );

function fuse_jobs_do_noposts() {

?>

<div class="entry">

<p>NO JOBS - EDIT THIS IN TAXONOMY-FUSE_JOBS_CATEGORY.PHP</p>

</div>




<?php

	// printf( '<div class="entry"><p>%s</p></div>', apply_filters( 'genesis_noposts_text', __( 'Sorry, no jobs matched your criteria.', 'genesis' ) ) );

}



remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );


// add_action ('genesis_before_loop','fuse_jobs_job_roller');



genesis();


/*
function genesis() {

	get_header();

	do_action( 'genesis_before_content_sidebar_wrap' );
	genesis_markup( array(
		'html5'   => '<div %s>',
		'xhtml'   => '<div id="content-sidebar-wrap">',
		'context' => 'content-sidebar-wrap',
	) );

		do_action( 'genesis_before_content' );
		genesis_markup( array(
			'html5'   => '<main %s>',
			'xhtml'   => '<div id="content" class="hfeed">',
			'context' => 'content',
		) );
			do_action( 'genesis_before_loop' );
			do_action( 'genesis_loop' );
			do_action( 'genesis_after_loop' );
		genesis_markup( array(
			'html5' => '</main>', //* end .content
			'xhtml' => '</div>', //* end #content
		) );
		do_action( 'genesis_after_content' );

	echo '</div>'; //* end .content-sidebar-wrap or #content-sidebar-wrap
	do_action( 'genesis_after_content_sidebar_wrap' );

	get_footer();

}
*/
