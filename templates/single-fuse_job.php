<?php


//* Enqueue Scripts
// add_action( 'wp_enqueue_scripts', 'fuse_job_scripts' );
function fuse_job_scripts() {}

/*
 * Use Single Job Sidebar
 *
**/ 
add_action( 'genesis_header','fuse_job_genesis_header' );
function fuse_job_genesis_header () {
	remove_action( 'genesis_sidebar', 'ss_do_sidebar' );
	remove_action( 'genesis_sidebar', 'genesis_do_sidebar' );
	add_action('genesis_sidebar','fuse_job_sidebar');
}

function fuse_job_sidebar(){
	dynamic_sidebar( 'sidebar-job-single' );
}



// Set Schema
// turn the schema type of each individual entry on the page into a job
add_filter( 'genesis_attr_entry', 'phut_schema_job', 20 );

// replace the normal “headline” itemprop for the entry title with “title”, as required by this specific schema
add_filter( 'genesis_attr_entry-title', 'phut_itemprop_title', 20 );

// change the itemprop of the entry content to “description”, instead of “text”, which is the default for a blog post;
add_filter( 'genesis_attr_entry-content', 'phut_itemprop_description', 20 );

// remove the overall schema.org type of the page as that would confuse the search engine.
add_filter( 'genesis_attr_content', 'phut_schema_empty', 20 );



//* Remove default loop
remove_action( 'genesis_loop', 'genesis_do_loop' );

//* Add custom loop
add_action( 'genesis_loop', 'fuse_job_loop' );
function fuse_job_loop(){

	if ( have_posts() ) :

		do_action( 'genesis_before_while' );
		

		while ( have_posts() ) : the_post();
	
			do_action( 'genesis_before_entry' );
			printf( '<article %s>', genesis_attr( 'entry' ) );
			do_action( 'genesis_entry_header' );



			do_action( 'genesis_entry_footer' );
			echo '</article>';
			do_action( 'genesis_after_entry' );
		
		endwhile; //* end of one post

		do_action( 'genesis_after_endwhile' );

	else : //* if no posts exist
		do_action( 'genesis_loop_else' );
	endif; //* end loop

} //fn

/* Schema
title
industry - Text
description
skills
occupationalCategory - Category or categories describing the job. Use BLS O*NET-SOC taxonomy: http://www.onetcenter.org/taxonomy.html. Ideally includes textual label and formal code, with the property repeated for each applicable value.
baseSalary
emloymentType - Type of employment (e.g. full-time, part-time, contract, temporary, seasonal, internship).
datePosted - Publication date for the job posting.
hiringOrganization
jobLocation


/*
 * Remove the post info function from the entry header
 *
**/
remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );



genesis();

