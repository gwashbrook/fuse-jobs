<?php

add_action( 'widgets_init', function(){
	register_widget( 'WP_Widget_Recent_Jobs' );
	register_widget( 'WP_Widget_Job_Categories' );	
});



/**
 * Recent_Jobs widget class
 *
 * 
 */
class WP_Widget_Recent_Jobs extends WP_Widget {

	public function __construct() {
		$widget_ops = array('classname' => 'widget_recent_jobs', 'description' => __( "Your site&#8217;s most recent Jobs.") );
		parent::__construct('recent-jobs', __('Recent Jobs'), $widget_ops);
		$this->alt_option_name = 'widget_recent_jobs';


		add_action( 'save_post_fuse_job', array($this, 'flush_widget_cache') );
		add_action( 'deleted_post', array($this, 'flush_widget_cache') ); //  NOT SURE ABOUT THIS ...  maybe deleted_post_fuse_job ??
		add_action( 'switch_theme', array($this, 'flush_widget_cache') );


	}

	public function widget($args, $instance) {
		$cache = array();
		if ( ! $this->is_preview() ) {
			$cache = wp_cache_get( 'widget_recent_jobs', 'widget' );
		}

		if ( ! is_array( $cache ) ) {
			$cache = array();
		}

		if ( ! isset( $args['widget_id'] ) ) {
			$args['widget_id'] = $this->id;
		}

		if ( isset( $cache[ $args['widget_id'] ] ) ) {
			echo $cache[ $args['widget_id'] ];
			return;
		}

		ob_start();

		$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'Recent Jobs' );

		/** This filter is documented in wp-includes/default-widgets.php */
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

		$number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 5;
		if ( ! $number )
			$number = 5;
		$show_date = isset( $instance['show_date'] ) ? $instance['show_date'] : false;
		$show_image = isset( $instance['show_image'] ) ? $instance['show_image'] : false;

		/**
		 * Filter the arguments for the Recent Jobs widget.
		 *
		 * @since 3.4.0
		 *
		 * @see WP_Query::get_posts()
		 *
		 * @param array $args An array of arguments used to retrieve the recent posts.
		 */
		$r = new WP_Query( apply_filters( 'widget_posts_args', array(
			'posts_per_page'      => $number,
			'no_found_rows'       => true,
			'post_status'         => 'publish',
			'ignore_sticky_posts' => true,
			
			'post_type' => 'fuse_job'
			
			
		) ) );

		if ($r->have_posts()) :
?>
		<?php echo $args['before_widget']; ?>
		<?php if ( $title ) {
			echo $args['before_title'] . $title . $args['after_title'];
		} ?>
		<ul>
		<?php while ( $r->have_posts() ) : $r->the_post(); ?>
			<li>
      
      
<?php
/*
if ( $show_image ) : ?>
				<a href="<?php the_permalink(); ?>"><?php echo genesis_get_image( array('size' => 'fuse-eap-sidebar') ) ?></a>


<?php endif;

*/
?>     
      
      
      
				<a href="<?php the_permalink(); ?>"><?php get_the_title() ? the_title() : the_ID(); ?></a>
			<?php if ( $show_date ) : ?>
				<span class="post-date">Added <?php echo get_the_date('jS M Y'); ?></span>
			<?php endif; ?>
			</li>
		<?php endwhile; ?>
		</ul>
		<?php echo $args['after_widget']; ?>
<?php
		// Reset the global $the_post as this query will have stomped on it
		wp_reset_postdata();

		endif;

		if ( ! $this->is_preview() ) {
			$cache[ $args['widget_id'] ] = ob_get_flush();
			wp_cache_set( 'widget_recent_jobs', $cache, 'widget' );
		} else {
			ob_end_flush();
		}
	}

	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['number'] = (int) $new_instance['number'];
		$instance['show_date'] = isset( $new_instance['show_date'] ) ? (bool) $new_instance['show_date'] : false;
		$instance['show_image'] = isset( $new_instance['show_image'] ) ? (bool) $new_instance['show_image'] : false;
		$this->flush_widget_cache();

		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if ( isset($alloptions['widget_recent_jobs']) )
			delete_option('widget_recent_jobs');

		return $instance;
	}

	public function flush_widget_cache() {
		wp_cache_delete('widget_recent_jobs', 'widget');
	}

	public function form( $instance ) {
		$title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$number    = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
		$show_date = isset( $instance['show_date'] ) ? (bool) $instance['show_date'] : false;
		$show_image = isset( $instance['show_image'] ) ? (bool) $instance['show_image'] : false;
?>
		<p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></p>

		<p><label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of jobs to show:' ); ?></label>
		<input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo $number; ?>" size="3" /></p>

		<p><input class="checkbox" type="checkbox" <?php checked( $show_date ); ?> id="<?php echo $this->get_field_id( 'show_date' ); ?>" name="<?php echo $this->get_field_name( 'show_date' ); ?>" />
		<label for="<?php echo $this->get_field_id( 'show_date' ); ?>"><?php _e( 'Display job date?' ); ?></label></p>

		<p><input class="checkbox" type="checkbox" <?php checked( $show_image ); ?> id="<?php echo $this->get_field_id( 'show_image' ); ?>" name="<?php echo $this->get_field_name( 'show_image' ); ?>" />
		<label for="<?php echo $this->get_field_id( 'show_image' ); ?>"><?php _e( 'Display cover image?' ); ?></label></p>

<?php
	}
}



/**
 * Job categories widget class
 *
 */
class WP_Widget_Job_Categories extends WP_Widget {

	public function __construct() {
		$widget_ops = array( 'classname' => 'widget_job_categories', 'description' => __( "A dropdown of job categories." ) );
		parent::__construct('job-categories', __('Job Categories Dropdown'), $widget_ops);
	}

	public function widget( $args, $instance ) {

		/** This filter is documented in wp-includes/default-widgets.php */
		$title = apply_filters( 'widget_title', empty( $instance['title'] ) ? false : $instance['title'], $instance, $this->id_base );


		$c = ! empty( $instance['count'] ) ? '1' : '0';
		$h = ! empty( $instance['hierarchical'] ) ? '1' : '0';

		echo $args['before_widget'];
		if ( $title ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}

		// $cat_args = array('orderby' => 'name', 'show_count' => $c, 'hierarchical' => $h);

		if ( is_tax( 'fuse_jobs_category' ) ) {
			$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
			$selected = $term->slug;
		} else {
			$selected = -1;
		}

		$cat_args = array (
			'show_option_none' => 'Browse Jobs by Sector',
			'show_count'       => $c,
			'hierarchical'     => $h,
      'hide_empty'       => 0,
      'taxonomy'         => 'fuse_jobs_category',
      'depth'            => 1,
      'orderby'          => 'name',
			'name'             => 'job-category-dropdown-widget',
			'value_field'      => 'slug',
			'selected'         => $selected,
		);

		wp_dropdown_categories( $cat_args  );
?>

<script type="text/javascript">
/* <![CDATA[ */
(function() {
var jobcatdropdown = document.getElementById("job-category-dropdown-widget");
function onJobCatChange() {
	if ( jobcatdropdown.options[jobcatdropdown.selectedIndex].value != -1 ) {
		location.href = "<?php echo esc_url( home_url( '/' ) ); ?>sector/"+jobcatdropdown.options[jobcatdropdown.selectedIndex].value;
	} else {
		location.href = "<?php echo esc_url( home_url( '/' ) ); ?>jobs-in-sussex/";
	}
}
jobcatdropdown.onchange = onJobCatChange;
})();
/* ]]> */
</script>

<?php


		echo $args['after_widget'];
	}

	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['count'] = !empty($new_instance['count']) ? 1 : 0;
		$instance['hierarchical'] = !empty($new_instance['hierarchical']) ? 1 : 0;
		return $instance;
	}

	public function form( $instance ) {
		//Defaults
		$instance = wp_parse_args( (array) $instance, array( 'title' => '') );
		$title = esc_attr( $instance['title'] );
		$count = isset($instance['count']) ? (bool) $instance['count'] :false;
		$hierarchical = isset( $instance['hierarchical'] ) ? (bool) $instance['hierarchical'] : false;
		
?>
		<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e( 'Title:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></p>

		<p><input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('count'); ?>" name="<?php echo $this->get_field_name('count'); ?>"<?php checked( $count ); ?> />
		<label for="<?php echo $this->get_field_id('count'); ?>"><?php _e( 'Show post counts' ); ?></label><br />

		<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('hierarchical'); ?>" name="<?php echo $this->get_field_name('hierarchical'); ?>"<?php checked( $hierarchical ); ?> />
		<label for="<?php echo $this->get_field_id('hierarchical'); ?>"><?php _e( 'Show hierarchy' ); ?></label></p>
<?php
	}

}







