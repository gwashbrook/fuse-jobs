<?php
/*
Plugin Name: Fuse Jobs
Description: Genesis friendly
Author:      Graham Washbrook
Author URI:  http://powerhut.tel
Version:     0.0.3
*/

if(!defined('ABSPATH')) exit;


require( plugin_dir_path( __FILE__ ) . 'inc/widgets/widgets.php');


add_action ('init', 'fuse_jobs_init' );
function fuse_jobs_init(){

	add_filter( 'post_type_link', 'fuse_jobs_post_type_permalink', 10, 2 );
	fuse_jobs_rewrite_rules ();
	fuse_jobs_register_job_taxonomies();	
	fuse_jobs_register_job_cpt();
	add_filter( 'enter_title_here', 'fuse_jobs_enter_title' );

} // fuse_jobs_init


/*
 * Register job custom post type
 */
function fuse_jobs_register_job_cpt() {

	$supports = array (
	
		'title',
		'excerpt',
		'editor',
		'author',
		'thumbnail',
		'custom-fields',
		// 'comments', // (also will see comment count balloon on edit screen)
		// 'revisions', // (will store revisions)
		// 'page-attributes',
		
		// GENESIS
		// 'genesis-simple-menus',
		// 'genesis-seo',
		// 'genesis-layouts',
		// 'genesis-simple-sidebars',
		// 'genesis-cpt-archives-settings',
		// 'genesis-entry-meta-before-content',
		
		// BUDDYPRESS
		// 'buddypress-activity',
		
		// THEME HYBRID
		// 'theme-layouts,
		
		// JETPACK
		// 'publicize',
		// 'wpcom-markdown',

	); // supports

	$labels = array ( // cool at 4.4
	
		'name'                  => 'Jobs',
		'singular_name'         => 'Job',
		'add_new'               => 'Add New',
	 	'add_new_item'          => 'Add New Job',		
		'edit_item'             => 'Edit Job',
		'menu_name'             => 'Jobs',
		'name_admin_bar'        => 'Job',
		'new_item'              => 'New Job',
		'view_item'             => 'View Job',
		'search_items'          => 'Search Jobs',
		'not_found'             => 'No jobs found',
		'not_found_in_trash'    => 'No jobs found in Trash',
		// 'parent_item_colon'  => 'Parent Job:', // only used in hierarchical post types
		'all_items'             => 'All Jobs',
		'archives'              => 'Job Archives',
		'insert_into_item'      => 'Insert into job',
		'uploaded_to_this_item' => 'Uploaded to this job',
		'featured_image'        => 'Featured image',
		'set_featured_image'    => 'Set featured image',
		'remove_featured_image' => 'Remove featured image',
		'use_featured_image'    => 'Use as featured image',
		'filter_items_list'     => 'Filter jobs list',
		'items_list_navigation' => 'Jobs list navigation',
		'items_list'            => 'Jobs list',
		
		// BUDDYPRESS
		// 'bp_activity_admin_filter' => '',
		// 'bp_activity_front_filter' => '',
 		// 'bp_activity_new_post'     => '',
 		// 'bp_activity_new_post_ms'  => '',

	);
	
	$rewrite = array (
		// 'slug'          => 'job/%job_id%',
		'with_front'    => false, // Should the permalink structure be prepended with the front base. (example: if your permalink structure is /blog/, then your links will be: false->/news/, true->/blog/news/). Defaults to true 
		'feeds'         => false, // Defaults to has_archive value
		'pages'         => false, // Should the permalink structure provide for pagination. Defaults to true
	);		
	
	$taxonomies = array (
		'fuse_jobs_category',
		'fuse_jobs_tag'
	);
	
/*	
	$bp_activity = array (
		'component_id' => '',
		'action_id'    => '',
		'contexts'     => array(),
		'position'     => 100,
	),
*/

	$args = array ( // cool at 4.4

		'labels'               => $labels,
		'description'          => 'A description for the CPT may go here', // (string) (optional) A short descriptive summary of what the post type is
		'public'               => true, // true || false
		'hierarchical'         => false,
		'exclude_from_search'  => false,	
		'publicly_queryable'   => true,
		'show_ui'	             => true,
		'show_in_menu'         => true,
		'show_in_nav_menus'    => true,
		'show_in_admin_bar'    => false,
		// 'menu_position'        => NULL, // Defauts to below Comments
		'menu_icon'            => 'dashicons-businessman', // The url to the icon to be used for this menu or the name of the icon from the iconfon
		'capability_type'      => 'post', // referenced
		// 'capabilities'         => $capabilities,
		// 'map_meta_cap'         => true,
		'supports'             => $supports,
		// 'register_meta_box_cb' => 'fuse_jobs_job_mb_cb', // Called when setting up the meta boxes for the edit form. The callback function takes one argument $post, which contains the WP_Post object for the currently edited post. Do remove_meta_box() and add_meta_box() calls in the callback. 
		'taxonomies'           => $taxonomies,
		'has_archive'          => true,
		'rewrite'              => $rewrite,		
		'query_var'             => true, // defaults to true (custom post type name)
		'can_export'            => true, // defaut true
		// 'delete_with_user' ??
		
		// BuddyPress
		// 'bp_activity'           => $bp_activity,
		
		// REST API
		'show_in_rest'          => true,
		'rest_base'             => 'jobs',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
		
	);
		
	register_post_type( 'fuse_job', $args );	
	
	
	
	// !!!!! Comment or Remove the following flush rewite rules when not in development !!!!!
	// See http://wordpress.org/support/topic/plugin-multi-page-toolkit-major-bad-practice-causes-60-second-page-loads
	// And Andrew Nacin's comment (senior WP dev ) here : http://wpengineer.com/2044/custom-post-type-and-permalink/
	
	flush_rewrite_rules();
	
}












/*
 * Use custom templates
 *
**/
add_filter('single_template', 'fuse_jobs_custom_templates');
add_filter('page_template', 'fuse_jobs_custom_templates');
add_filter('archive_template', 'fuse_jobs_custom_templates');
add_filter('taxonomy_template', 'fuse_jobs_custom_templates');

function fuse_jobs_custom_templates( $template ) {

	if ( is_singular('fuse_job') ) {
		return plugin_dir_path( __FILE__ ) . 'templates/single-fuse_job.php';
	}

	if ( is_page( 'jobs-in-sussex' ) ) {
		return plugin_dir_path( __FILE__ ) . 'templates/archive-fuse_job.php';
	}

/*
	if ( is_post_type_archive( 'fuse_job' ) ) {
		return plugin_dir_path( __FILE__ ) . 'templates/archive-fuse_job.php';
	}
*/

	if ( is_tax('fuse_jobs_category'    ) ) {
		return plugin_dir_path( __FILE__ ) . 'templates/taxonomy-fuse_jobs_category.php';
	}

	return $template;

}


/*
 * Register custom sidebars
 *
 *
**/
add_action('genesis_setup','fuse_jobs_register_genesis_sidebars',11);

function fuse_jobs_register_genesis_sidebars(){

	genesis_register_sidebar( array(
		'id'          => 'sidebar-job-single',
		'name'        => 'Job - Single',
		'description' => 'This is the sidebar for single job pages.',
	) );

	genesis_register_sidebar( array(
		'id'          => 'sidebar-job-taxonomy',
		'name'        => 'Job - Taxonomy',
		'description' => 'This is the sidebar for taxonomy job pages.',
	) );

}


/*
 * Add image sizes ????
 *
**/




/*
 * Filter post_type_permalink to include post id
 *
**/
function fuse_jobs_post_type_permalink( $url, $post ){

	if ( ( strpos( '%job_id%', $url ) === 'TRUE' ) || ( 'fuse_job' == get_post_type( $post ) ) ) {
		return get_home_url() . '/job/' . $post->ID . '/';
	} else {
		return $url;
	}

}



// Register taxonomies, called in init
function fuse_jobs_register_job_taxonomies(){
	
	$category_args = array (
	
		'labels' => array ( 

			'name'                       => 'Job Categories',
			'singular_name'              => 'Job Category',
			'search_items'               => 'Search Job Categories',
			'popular_items'              => 'Popular Job Categories',
			'all_items'                  => 'All Categories',
			'parent_item'                => 'Parent Category',
			'parent_item_colon'          => 'Parent Category:',
			'edit_item'                  => 'Edit Category',		
			'view_item'                  => 'View Category',
			'update_item'                => 'Update Category',
			'add_new_item'               => 'Add New Job Category',
			'new_item_name'              => 'New Category',
			'separate_items_with_commas' => 'Separate categories with commas',			
			'add_or_remove_items'        => 'Add or remove categories',
			'choose_from_most_used'      => 'Choose from the most used categories',		
			'not_found'                  => 'No categories found.',
			'no_terms'                   => 'No categories',
			'items_list_navigation'      => 'Categories list navigation',
			'items_list'                 => 'Categories list',
			'menu_name'                  => 'Job Categories', // default same as 'name'

		),
		'public' => true,
		//'public' => false, // HIDE THE ARCHIVE
		
		'show_ui' => true,            // defaults to value of public argument
		// 'show_in_nav_menus' => true,  // defaults to value of public argument
		'show_in_nav_menus' => false, // DON'T NEED THEM, SO HIDE
		
		'show_tagcloud' => false, //  Whether to allow the Tag Cloud widget to use this taxonomy, defaults to value of show_ui
		
		'show_in_quick_edit' => true, // defaults to value of show_ui
		
		
		// 'meta_box_cb'
		'show_admin_column' => true, // automatic creation of taxonomy columns on associated posts table
		
		
		
		'hierarchical' => true,
		// 'update_count_callback' => //  (string) (optional) A function name that will be called to update the count of an associated $object_type, such as post, is updated. Default: None 

		// 'query_var' => // (boolean or string) (optional) False to disable the query_var, set as string to use custom query_var instead of default which is $taxonomy, the taxonomy's "name". Default: $taxonomy
		'query_var' => true,
		
		
		// 'query_var' => false,
		
		/*
		Note:
		The query_var is used for direct queries through WP_Query like new WP_Query(array('people'=>$person_name))
		and URL queries like /?people=$person_name. Setting query_var to false will disable these methods, but you
		can still fetch posts with an explicit WP_Query taxonomy query like WP_Query(array('taxonomy'=>'people', 'term'=>$person_name)).
		*/
		
		
		'rewrite' => array( // (boolean or array) (optional) Set to false to prevent automatic URL rewriting a.k.a. "pretty permalinks". Pass an $args array to override default URL settings for permalinks as outlined below. Default: true 
			'slug' => 'sector', // - Used as pretty permalink text (i.e. /tag/) - defaults to $taxonomy (taxonomy's name slug)
			'with_front' => true, // - allowing permalinks to be prepended with front base - defaults to true
			'hierarchical' => true// - true or false allow hierarchical urls (implemented in Version 3.1) 
		),
		
		
		// 'rewrite' => false,
		
		// 'capabilities' => array 
		// 'sort' => false, // default none // Whether this taxonomy should remember the order in which terms are added to objects
		// 'show_in_rest'       => true,
		// 'rest_base'          => 'genre',
		// 'rest_controller_class' => 'WP_REST_Terms_Controller',
	
		
		
		
		
	);
	
	
	register_taxonomy( 'fuse_jobs_category', array( 'fuse_job' ), $category_args );




	$tag_args = array(
	
		'labels' => array ( // Cool at 4.4
		
			'name'                       => 'Job Tags',
			'singular_name'              => 'Job Tag',
			'search_items'               => 'Search Job Tags',
			'popular_items'              => 'Popular Job Tags',
			'all_items'                  => 'All Tags',
			// 'parent_item'                => 'Parent Tag',
			// 'parent_item_colon'          => 'Parent Tag:',
			'edit_item'                  => 'Edit Tag',		
			'view_item'                  => 'View Tag',
			'update_item'                => 'Update Tag',
			'add_new_item'               => 'Add New Job Tag',
			'new_item_name'              => 'New Tag',
			'separate_items_with_commas' => 'Separate tags with commas',			
			'add_or_remove_items'        => 'Add or remove tags',
			'choose_from_most_used'      => 'Choose from the most used tags',		
			'not_found'                  => 'No tags found.',
			'no_terms'                   => 'No tags',
			'items_list_navigation'      => 'Tags list navigation',
			'items_list'                 => 'Tags list',
			'menu_name'                  => 'Job Tags', // default same as 'name'
		),
		'public' => true,
		//'public' => false, // HIDE THE ARCHIVE
		
		'show_ui' => true,            // defaults to value of public argument
		// 'show_in_nav_menus' => true,  // defaults to value of public argument
		'show_in_nav_menus' => false, // DON'T NEED THEM, SO HIDE
		
		'show_tagcloud' => true, //  Whether to allow the Tag Cloud widget to use this taxonomy, defaults to value of show_ui
		
		'show_in_quick_edit' => true, // defaults to value of show_ui
		
		
		// 'meta_box_cb'
		'show_admin_column' => true, // automatic creation of taxonomy columns on associated posts table
		
		
		
// 		'hierarchical' => true,
		'hierarchical' => false,
		// 'update_count_callback' => //  (string) (optional) A function name that will be called to update the count of an associated $object_type, such as post, is updated. Default: None 

		// 'query_var' => // (boolean or string) (optional) False to disable the query_var, set as string to use custom query_var instead of default which is $taxonomy, the taxonomy's "name". Default: $taxonomy
		'query_var' => true,
		
		
		// 'query_var' => false,
		
		/*
		Note:
		The query_var is used for direct queries through WP_Query like new WP_Query(array('people'=>$person_name))
		and URL queries like /?people=$person_name. Setting query_var to false will disable these methods, but you
		can still fetch posts with an explicit WP_Query taxonomy query like WP_Query(array('taxonomy'=>'people', 'term'=>$person_name)).
		*/
		
		
		'rewrite' => array( // (boolean or array) (optional) Set to false to prevent automatic URL rewriting a.k.a. "pretty permalinks". Pass an $args array to override default URL settings for permalinks as outlined below. Default: true 
			'slug' => 'jtag', // - Used as pretty permalink text (i.e. /tag/) - defaults to $taxonomy (taxonomy's name slug)
			'with_front' => true, // - allowing permalinks to be prepended with front base - defaults to true
			'hierarchical' => false// - true or false allow hierarchical urls (implemented in Version 3.1) 
		),
		
		
		// 'rewrite' => false,
		
		// 'capabilities' => array 
		// 'sort' => false, // default none // Whether this taxonomy should remember the order in which terms are added to objects
		
		// 'show_in_rest'       => true,
		// 'rest_base'          => 'genre',
		// 'rest_controller_class' => 'WP_REST_Terms_Controller',
		
		
		
	);

	register_taxonomy( 'fuse_jobs_tag', array( 'fuse_job' ), $tag_args );
	
	flush_rewrite_rules( false );
	
} //fn


/*
 *
 *
**/
function fuse_jobs_enter_title( $title ){
	$screen = get_current_screen();
	if( $screen->post_type == 'fuse_job' ) 
		$title = 'Enter job title here';
	return $title;
}




/*
 * 	// Prevent WordPress from sending a 404 for our new perma structure.
 */
function fuse_jobs_rewrite_rules () {

	// e.g. http://domain.com/job/282/ -> http://domain.com/index.php?post_type=fuse_job&p=282
	add_rewrite_rule( '^job\/(\d+)\/?$','index.php?post_type=fuse_job&p=$matches[1]', 'top' );

}


/*
 *
 *
**/
add_filter('wp_list_categories', 'fuse_jobs_list_categories', 10, 2);
function fuse_jobs_list_categories( $output, $args ){
	if ( isset( $args['taxonomy'] ) && $args['taxonomy'] == 'fuse_jobs_category' ) {
		$search  = array( '<ul>', '</ul>', '<li', '</li>' );
		$replace = array( '', '', '<div', '</div>' );
		$output = str_replace ( $search , $replace , $output );
	}
	return $output;
}

/*
 *
 *
**/
function fuse_jobs_category_list() {
	?>
<div class="browse-job-categories">
	<h1>Browse jobs by sector</h1>
	<div class="job-category-list clearfix">
			<?php
      $args = array(
        'title_li'    => '',	
        'show_count'  => 1,
        'hide_empty'  => 0,
        'taxonomy'    => 'fuse_jobs_category',
        'depth'       => 1,
        'orderby'     => 'name',
      );
      wp_list_categories( $args );
      ?>
	</div>

	<div class="job-category-dropdown">
		<?php
			if ( is_tax( 'fuse_jobs_category' ) ) {
				$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
				$selected = $term->slug;
			} else {
				$selected = -1;
			}
			$args = array (
				'show_option_none' => 'Browse jobs by sector',
				'show_count'  => 1,
				'hide_empty'  => 0,
				'taxonomy'    => 'fuse_jobs_category',
				'depth'       => 1,
				'orderby'     => 'name',
				'name'        => 'job-category-dropdown',
				'value_field' => 'slug',
				'selected'    => $selected,
			);
			wp_dropdown_categories( $args );
		?>
<script type="text/javascript">
/* <![CDATA[ */
(function() {
var dropdown = document.getElementById("job-category-dropdown");
function onCatChange() {
	if ( dropdown.options[dropdown.selectedIndex].value != -1 ) {
		location.href = "<?php echo esc_url( home_url( '/' ) ); ?>sector/"+dropdown.options[dropdown.selectedIndex].value;
	} else {
		location.href = "<?php echo esc_url( home_url( '/' ) ); ?>jobs-in-sussex/";
	}
}
dropdown.onchange = onCatChange;
})();
/* ]]> */
</script>
	</div>
</div>
<?php
}


/*
 *
 *
 * See http://www.studiopress.com/forums/topic/how-to-add-elements-to-individual-pages/#post-153361
 * See https://codex.wordpress.org/Class_Reference/WP_Query#Taxonomy_Parameters
 * See http://wordpress.stackexchange.com/questions/45901/passing-a-parameter-to-filter-and-action-functions
 *
**/
add_action ('genesis_meta','maybe_fuse_jobs_job_roller');
function maybe_fuse_jobs_job_roller(){
	
	if(  is_tax( 'fuse_jobs_category' ) || is_post_type_archive( 'fuse_job' ) || is_page('jobs-in-sussex') ) { 

		$args = array (
			'posts_per_page'      => -1,
			'no_found_rows'       => true,
			'post_status'         => 'publish',
			'ignore_sticky_posts' => true,
			'post_type'           => 'fuse_job',
			'nopaging'            => true,
			'orderby'             => 'rand',
			'tax_query' => array(
				array(
					'taxonomy' => 'fuse_jobs_tag',
					'field'    => 'slug',
					'terms'    => 'featured',
				),	
			),		
		);

		if( is_tax( 'fuse_jobs_category' ) ) {
	
			$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
	
			$args['tax_query'] = array (
			
				'relation' => 'AND',	
				array(
					'taxonomy' => 'fuse_jobs_tag',
					'field'    => 'slug',
					'terms'    => 'featured',
				),	
				array(
					'taxonomy' => 'fuse_jobs_category',
					'field'    => 'slug',
					'terms'    => $term->slug,
				),		
			
			);
			
		} // if job category taxonomy archive
	
		$roller = new WP_Query( $args );
	



		ob_start();
		?>
    <div style="position:relative">

    	<div class="fuse-info" style="position:absolute;z-index:1;right:10px;">
      	<a href="https://fusesussex.co.uk/jobs-in-sussex/advertising-jobs-with-fusesussex/"><i class="fa fa-info-circle"></i></a>
      </div>

    	<div id="job-roller">

		

    <?php
		if ($roller->have_posts()) :

			// Tee up the jQuery slick scripts
			add_action( 'wp_enqueue_scripts', 'fuse_jobs_job_roller_scripts' );
			add_action( 'genesis_footer','fuse_jobs_job_roller_js' );		

			// Loop through the featured jobs and build job-roller html
			while ( $roller->have_posts() ) : $roller->the_post(); ?>

        <div class="featured-job">
          <h2><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
          <p>COMPANY - Location</p>
          <p><?php the_date('jS M Y'); ?></p>
        </div>

			<?php endwhile; ?>

			<?php wp_reset_postdata(); ?>
      
      <div class="featured-job promote">
        <h2><a href="https://fusesussex.co.uk/jobs-in-sussex/advertising-jobs-with-fusesussex/" title="Promote your job">Promote Your Job</a></h2>
        <p>It's Easy</p>
        <p><?php echo date('jS M Y'); ?></p>
        <?php // the_excerpt(); ?>
      </div>


		<?php else: ?>
    
    	PROMOTE YOUR JOB HERE

		<?php endif;	?>
 
    
    </div>
  </div>

<?php

	$html = ob_get_clean();
	$html = trim( preg_replace('#>\s+<#s', '><', $html) );



	// Add the html somewhere useful on page
	

	if( is_page('jobs-in-sussex') ) {
		add_action ('genesis_before_entry_content', function() use ( $html ) { echo $html; }, 10);
	} else { 
		add_action ('genesis_before_loop', function() use ( $html ) { echo $html; }, 99);	
	}
	
	} // only if jobs category taxonomy, or jobs archive page
	
} // maybe_fuse_jobs_job_roller





/*
 *
 *
**/
function fuse_jobs_job_roller_scripts() {
	wp_enqueue_style( 'jq-slick', '//cdn.jsdelivr.net/jquery.slick/1.5.8/slick.css', array(), '1.5.8' );
	wp_enqueue_style( 'jq-slick-theme', '//cdn.jsdelivr.net/jquery.slick/1.5.8/slick-theme.css', array(), '1.5.8' );
	wp_enqueue_script( 'jq-slick-js', '//cdn.jsdelivr.net/jquery.slick/1.5.8/slick.min.js', array( 'jquery' ), '1.0.0', true );
}


/*
 *
 *
**/
function fuse_jobs_job_roller_js () {
?>
<script>
jQuery(document).ready(function($) {
	$('#job-roller').slick({
		autoplay: true,
		dots: true,
	  // infinite: true,
  	slidesToShow: 3,
  	slidesToScroll: 1,
		arrows: false,
		responsive: [
			{
				breakpoint: 786,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
					infinite: true,
					dots: false,
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					infinite: true,
					dots: false,
				}
			},			
		]
	});
});
</script>
<?php
}

/*
 *
 *
**/
function fuse_jobs_job_roller ( ) {
	
	$args = array (

		'posts_per_page'      => -1,
		'no_found_rows'       => true,
		'post_status'         => 'publish',
		'ignore_sticky_posts' => true,
		'post_type'           => 'fuse_job',
		'nopaging'            => true,
		'orderby'             => 'rand',
		'tax_query' => array(
			
			array(
				'taxonomy' => 'fuse_jobs_tag',
				'field'    => 'slug',
				'terms'    => 'featured',
			),	
	
		),		
	);
	
	if( is_tax( 'fuse_jobs_category' ) ) {

		$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );

		$args['tax_query'] = array (
		
			'relation' => 'AND',	
			array(
				'taxonomy' => 'fuse_jobs_tag',
				'field'    => 'slug',
				'terms'    => 'featured',
			),	
			array(
				'taxonomy' => 'fuse_jobs_category',
				'field'    => 'slug',
				'terms'    => $term->slug,
			),		
		
		);
		
	} // if job category taxonomy archive
	
	$roller = new WP_Query( $args );

	if ( $roller->have_posts() ) :
?>
<div id="job-roller">
<?php while ( $roller->have_posts() ) : $roller->the_post(); ?>
<div><a href="<?php the_permalink(); ?>"><?php get_the_title() ? the_title() : the_ID(); ?></a></div>
<?php endwhile; ?>
</div>
<?php	
	wp_reset_postdata();
	endif;	
}